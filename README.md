# Permissions Tree in JavaScript

This is an implementation of a permissions tree in JavaScript using ES6+ syntax.

## Overview

This is a simple, but powerful, data structure that can be retrofitted to any JavaScript app with a hierarchical permissions system. Say you have a Learning Management System (LMS) with the following structure:

**NOTE: This structure is a simplified example**

LMS (root) <br>
&emsp;&emsp;|-------- Courses <br>
&emsp;&emsp;|&emsp;&emsp;&emsp;&emsp;&emsp;|-------Enrollees <br>
&emsp;&emsp;|&emsp;&emsp;&emsp;&emsp;&emsp;|-------Course Works <br>
&emsp;&emsp;|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;|------- Readings <br>
&emsp;&emsp;|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;|------- Quizzes <br>
&emsp;&emsp;|<br>
&emsp;&emsp;|-------- Users <br>
&emsp;&emsp;&nbsp;&emsp;&emsp;&emsp;&emsp;&emsp;|-------Students <br>
&emsp;&emsp;&nbsp;&emsp;&emsp;&emsp;&emsp;&emsp;|-------Teachers <br>
&emsp;&emsp;&nbsp;&emsp;&emsp;&emsp;&emsp;&emsp;|-------Admins <br>

A hierarchical permissions system means that a user with full rights to the root will have full rights to ALL the nodes of this permissions tree. A user that has full rights to the the "Courses" node will have full rights to the "Enrollees", "Course Works", "Readings", and "Quizzes" node. What "full rights" actually means depends on the context of your app. In this implementation, "full rights" is defined as being able to Create, Delete, Edit, and View a node. 

# How to Use

